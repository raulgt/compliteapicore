﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CompleteApiCoreEntity.Common.DataBase.Class;
using CompleteApiCoreEntity.Common.Products.Classes;
using CompleteApiCoreEntity.Common.Products.Interface.Service;
using CompleteApiCoreEntity;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Internal;
using System.Globalization;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace CompleteApiCore.Controllers
{
    [EnableCors("AllowMyOrigin")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IServiceProducts serviceProducts;
        private readonly IOperationResult op;

        public static IHostingEnvironment environment;

        readonly DBEntitiesContext dbContext;

        public ProductsController(IOperationResult _op, 
                                  IServiceProducts _serviceProducts, 
                                  IHostingEnvironment _environment                                )
        {
            op = _op;
            serviceProducts = _serviceProducts; 
            environment = _environment;
        }

     

        // GET: api/Products/azure
        [HttpGet("azure")]
        public async Task<IOperationResult> GetProductsTableStore()
        {
            var nextRowKey = Request.Headers["nextRowKey"];
            var nextPartitionKey = Request.Headers["nextPartitionKey"];
            int bunch = Convert.ToInt32(Request.Headers["bunch"]);

            return await serviceProducts.GetProductsTableStore(nextRowKey, nextPartitionKey, bunch);
        }

        // GET: api/Products/azure
        [HttpGet("azure/{rowKey}")]
        public async Task<IOperationResult> GetProductTableStore([FromRoute] string rowKey)
        {
            return await serviceProducts.GetProductTableStore(rowKey);
        }
               
        // POST: api/Products/azure  
        [HttpPost("azure")]
        public async Task<IOperationResult> PostProductTableStore()
        {
            try
            {
               
                ProductAzure product = new ProductAzure(); 
                var imageFile = Request.Form.Files[0];

                product.Nombre = Request.Form["nombre"];
                product.Caracteristicas = Request.Form["caracteristicas"];
                product.Precio = Convert.ToDouble(Request.Form["precio"], CultureInfo.InvariantCulture);
                product.FechaLanzamiento = DateTime.ParseExact(Request.Form["fechaLanzamiento"].ToString(), "yyyy/MM/dd hh:mm:ss", CultureInfo.InvariantCulture);
                product.CorreoFabricante = Request.Form["correoFabricante"];
                product.Pais = Request.Form["pais"];
                product.UnidadesDisponibles = Convert.ToInt32(Request.Form["unidadesDisponibles"]);
                product.UnidadesVendidas = Convert.ToInt32(Request.Form["unidadesVendidas"]);
                product.Revision = (Request.Form["revision"] == "true") ? true : false;
                product.FechaUltimaRevision = DateTime.Now;



                if (imageFile.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        await imageFile.CopyToAsync(stream);
                        var byteArray = stream.ToArray();
                        //product.ImagenFileTable = System.Text.Encoding.UTF8.GetString(byteArray);
                        product.ImagenFileTable = byteArray;
                    }
                }


                return await serviceProducts.CreateProductTableStore(product);
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }


        // DELETE: api/Products/5/azure
        [HttpDelete("azure/{rowKey}")]
        public async Task<IOperationResult> DeleteProductTableStore([FromRoute] string rowKey)
        {
            return await serviceProducts.DeleteProductTableStore(rowKey);
        }
             
        


        
        /// ////////////////////////////////////////////////////////////////////////////
        ///                    CRUD SQLSERVER
        /////////////////////////////////////////////////////////////////////////

        // GET: api/Products
        [HttpGet]
        public async Task<IOperationResult> GetProducts()
        {
            return await serviceProducts.GetProducts();
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<IOperationResult> GetProduct([FromRoute] int id)
        {
            return await serviceProducts.GetProduct(id);
        }


        // POST: api/Products  
        [HttpPost]
        public async Task<IOperationResult> PostProduct()
        {
            try
            {
                Product product = new Product();
               

                var imageFile = Request.Form.Files[0];

                product.Nombre = Request.Form["nombre"];

                product.Caracteristicas = Request.Form["caracteristicas"];
                product.Precio = Convert.ToDouble(Request.Form["precio"], CultureInfo.InvariantCulture);
                product.FechaLanzamiento = DateTime.ParseExact(Request.Form["fechaLanzamiento"].ToString(), "yyyy/MM/dd hh:mm:ss", CultureInfo.InvariantCulture);
                product.CorreoFabricante = Request.Form["correoFabricante"];
                product.Pais = Request.Form["pais"];
                product.UnidadesDisponibles = Convert.ToInt32(Request.Form["unidadesDisponibles"]);
                product.UnidadesVendidas = Convert.ToInt32(Request.Form["unidadesVendidas"]);


                if (imageFile.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        await imageFile.CopyToAsync(stream);
                        product.ImagenFile = stream.ToArray();
                       
                    }
                }
             
             
                return await serviceProducts.CreateProduct(product);
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IOperationResult> DeleteProduct([FromRoute] int id)
        {
            return await serviceProducts.DeleteProduct(id);
        }

    }
}