﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CompleteApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InitialController : ControllerBase
    {

        // GET: api/Initial
        [HttpGet("default/")]
        public String GetDefault()
        {           
            return "SERVICIO FUNCIONANDO..!!!";
        }
    }
}