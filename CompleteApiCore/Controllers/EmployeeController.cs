﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CompleteApiCoreEntity;
using CompleteApiCoreEntity.Common.Employees.Classes;
using CompleteApiCoreEntity.Common.Employees.Interface.Service;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace CompleteApiCore.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IServiceEmployee _serviceEmployee;
        private readonly IValidator<Employee> _validator;
        private readonly IOperationResult op_;


        public EmployeeController(IServiceEmployee serviceEmployee,
                                  IValidator<Employee> validator,
                                  IOperationResult op)
        {
            _serviceEmployee = serviceEmployee;
            _validator = validator;
            op_ = op;
        }

        // GET api/Employees
        [HttpGet]
        public IOperationResult Get()
        {

            return _serviceEmployee.GetEmployees();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public IOperationResult Post([FromBody] Employee employee)
        {
            try
            {
                ValidationResult results = _validator.Validate(employee);
                string error = string.Empty;


                if (!results.IsValid)
                {
                    foreach (var failure in results.Errors)
                    {
                        error += "Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage;
                    }

                    throw new Exception(error);
                }

                return _serviceEmployee.SaveEmployee(employee);

            }
            catch (Exception e)
            {
                op_.GetErrorOperation(e);
            }

            return op_;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
