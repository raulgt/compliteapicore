﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CompleteApiCoreEntity.Common.DataBase.Class;
using CompleteApiCoreEntity.Common.Opportunity.Classes;
using CompleteApiCoreEntity.Common.Opportunity.Interface.Service;
using CompleteApiCoreEntity;

namespace CompleteApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OpportunityController : ControllerBase
    {
        private readonly IServiceOpportunity serviceOpportunity;
        private readonly IOperationResult op;

        public OpportunityController(IOperationResult _op, IServiceOpportunity _serviceOpportunity)
        {
            serviceOpportunity = _serviceOpportunity;
            op = _op;
        }

        // GET: api/Opportunity
        [HttpGet]
        public async Task<IOperationResult> GetStatusOppportunity()
        {
            return await serviceOpportunity.GetStatusOpportunity();
        }
               
        // PUT: api/Opportunity/carlos
        [HttpPut("{user}")]
        public async Task<IOperationResult> ChangueStatusOpportunity([FromRoute] string user)
        {
            return await serviceOpportunity.ChangeStatusOpportunity(user);
        }  

    }
}