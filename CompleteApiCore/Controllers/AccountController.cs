﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CompleteApiCoreEntity;
using CompleteApiCoreEntity.Common.DataBase.Class;
using CompleteApiCoreEntity.Common.Users.Clasess;
using CompleteApiCoreEntity.Common.Users.Interface.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CompleteApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {       
        private readonly IOperationResult op;
        private readonly IServiceAccount serviceAccount;

        public AccountController(IOperationResult _op, IServiceAccount _serviceAccount)
        {
            op = _op;
            serviceAccount = _serviceAccount;
        }

        [Route("Create")]
        [HttpPost]
        public async Task<IOperationResult> CreateUser([FromBody] UserInfo userInfo)
        {          

            if (ModelState.IsValid)
            {
                return await serviceAccount.CreateUser(userInfo);     
            }
            else
            {
                op.status = HttpStatusCode.BadRequest;
                return op;
            }
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IOperationResult> Login([FromBody] UserInfo userInfo)
        {

            if (ModelState.IsValid)
            {
                return await serviceAccount.LoginUser(userInfo);
            }
            else
            {                
                op.status = HttpStatusCode.BadRequest;
                return op;
            }
        }

       
    }
}