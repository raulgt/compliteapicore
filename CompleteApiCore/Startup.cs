﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompleteapiCoreBussines.Common;
using CompleteApiCoreDataAccess.Common.Employees;
using CompleteApiCoreDataAccess.Common.HostedServices;
using CompleteApiCoreDataAccess.Common.Hubs;
using CompleteApiCoreEntity;
using CompleteApiCoreEntity.Common.DataBase.Class;
using CompleteApiCoreEntity.Common.Employees.Classes;
using CompleteApiCoreEntity.Common.Employees.Classes.Validators;
using CompleteApiCoreEntity.Common.Employees.Interface.Data;
using CompleteApiCoreEntity.Common.Employees.Interface.Service;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace CompleteApiCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // services.AddTransient<Microsoft.Extensions.Hosting.IHostedService, UpdateRevisionDate>();
            services.AddTransient<Microsoft.Extensions.Hosting.IHostedService, UpdateRevisionDateDbContex>();

            //services.AddDbContext<DBEntitiesContext>(options =>
            //options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("CompleteApiCore")));           

            services.AddDataAccessServices(Configuration.GetConnectionString("DefaultConnection"));

            // Agregamos identificacion de usuario y roles en relacion con el DbContext
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<DBEntitiesContext>()
                .AddDefaultTokenProviders();

            // Agregamos Json web token authentication
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
              .AddJwtBearer(options =>
               options.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuer = true,
                   ValidateAudience = true,
                   ValidateLifetime = true,
                   ValidateIssuerSigningKey = true,
                   ValidIssuer = "yourdomain.com",
                   ValidAudience = "yourdomain.com",
                   IssuerSigningKey = new SymmetricSecurityKey(
                  Encoding.UTF8.GetBytes(Configuration["Llave_secreta"])),
                   ClockSkew = TimeSpan.Zero
               });


            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyOrigin",
                builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            });

            // Agregamos SignalR
            services.AddSignalR();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScopedServices();

            services.AddScopedValidationServices();        

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }



            app.UseStaticFiles();
            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseCors("AllowMyOrigin");


            // Ruta a un Hub
            app.UseSignalR(x => {
                x.MapHub<OpportunityHub>("/opportunityHub");

                // Aqui podriamos agregar otros Hubs
                // Ejemplo: x.MapHub<ClaseHub>("/RutaHub en Javascript");
            });

            app.UseMvc();


        }
    }
}
