﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompleteApiCore.DataMigrations
{
    public partial class logicaGuardarImagenes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Imagen",
                table: "Products",
                newName: "ImagenFile");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ImagenFile",
                table: "Products",
                newName: "Imagen");
        }
    }
}
