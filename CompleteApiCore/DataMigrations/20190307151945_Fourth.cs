﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompleteApiCore.DataMigrations
{
    public partial class Fourth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_AbilitiEmployees_Id",
                table: "AbilitiEmployees");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "AbilitiEmployees");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "AbilitiEmployees",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_AbilitiEmployees_Id",
                table: "AbilitiEmployees",
                column: "Id");
        }
    }
}
