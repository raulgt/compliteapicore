﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CompleteApiCore.DataMigrations
{
    public partial class AgregandoProductos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_employeeExpedients_Employees_EmployeeId",
                table: "employeeExpedients");

            migrationBuilder.DropPrimaryKey(
                name: "PK_employeeExpedients",
                table: "employeeExpedients");

            migrationBuilder.RenameTable(
                name: "employeeExpedients",
                newName: "EmployeeExpedients");

            migrationBuilder.RenameIndex(
                name: "IX_employeeExpedients_EmployeeId",
                table: "EmployeeExpedients",
                newName: "IX_EmployeeExpedients_EmployeeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployeeExpedients",
                table: "EmployeeExpedients",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Caracteristicas = table.Column<string>(nullable: true),
                    FechaLanzamiento = table.Column<DateTime>(nullable: false),
                    CorreoFabricante = table.Column<string>(nullable: true),
                    Pais = table.Column<string>(nullable: true),
                    UnidadesDisponibles = table.Column<int>(nullable: false),
                    UnidadesVendidas = table.Column<int>(nullable: false),
                    Imagen = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeExpedients_Employees_EmployeeId",
                table: "EmployeeExpedients",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeExpedients_Employees_EmployeeId",
                table: "EmployeeExpedients");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployeeExpedients",
                table: "EmployeeExpedients");

            migrationBuilder.RenameTable(
                name: "EmployeeExpedients",
                newName: "employeeExpedients");

            migrationBuilder.RenameIndex(
                name: "IX_EmployeeExpedients_EmployeeId",
                table: "employeeExpedients",
                newName: "IX_employeeExpedients_EmployeeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_employeeExpedients",
                table: "employeeExpedients",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_employeeExpedients_Employees_EmployeeId",
                table: "employeeExpedients",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
