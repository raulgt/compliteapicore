﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CompleteApiCore.DataMigrations
{
    public partial class AgregandoRevisionyFechaRevision : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "FechaUltimaRevision",
                table: "Products",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "Revision",
                table: "Products",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FechaUltimaRevision",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Revision",
                table: "Products");
        }
    }
}
