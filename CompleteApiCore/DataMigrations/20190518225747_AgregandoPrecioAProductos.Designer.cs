﻿// <auto-generated />
using System;
using CompleteApiCoreEntity.Common.DataBase.Class;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CompleteApiCore.DataMigrations
{
    [DbContext(typeof(DBEntitiesContext))]
    [Migration("20190518225747_AgregandoPrecioAProductos")]
    partial class AgregandoPrecioAProductos
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.8-servicing-32085")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CompleteApiCoreEntity.Common.Abilities.Classes.AbilitiEmployee", b =>
                {
                    b.Property<int>("AbilityId");

                    b.Property<int>("EmployeeId");

                    b.HasKey("AbilityId", "EmployeeId");

                    b.HasIndex("EmployeeId");

                    b.ToTable("AbilitiEmployees");
                });

            modelBuilder.Entity("CompleteApiCoreEntity.Common.Abilities.Classes.Ability", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Abilities");
                });

            modelBuilder.Entity("CompleteApiCoreEntity.Common.Deparment.Classes.Department", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description");

                    b.HasKey("Id");

                    b.ToTable("Deparments");
                });

            modelBuilder.Entity("CompleteApiCoreEntity.Common.Employees.Classes.Employee", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address");

                    b.Property<DateTime>("BirthDate");

                    b.Property<int?>("DepartmentId");

                    b.Property<string>("FirstName");

                    b.Property<DateTime>("HireDate");

                    b.Property<string>("LastName");

                    b.Property<string>("Notes")
                        .HasMaxLength(300);

                    b.Property<string>("Position");

                    b.Property<string>("Prefix");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("CompleteApiCoreEntity.Common.Employees.Classes.EmployeeExpedient", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("EmployeeId");

                    b.Property<string>("Expedient");

                    b.Property<string>("Nationality");

                    b.HasKey("Id");

                    b.HasIndex("EmployeeId")
                        .IsUnique();

                    b.ToTable("EmployeeExpedients");
                });

            modelBuilder.Entity("CompleteApiCoreEntity.Common.Products.Classes.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Caracteristicas");

                    b.Property<string>("CorreoFabricante");

                    b.Property<DateTime>("FechaLanzamiento");

                    b.Property<byte>("Imagen");

                    b.Property<string>("Nombre");

                    b.Property<string>("Pais");

                    b.Property<double>("Precio");

                    b.Property<int>("UnidadesDisponibles");

                    b.Property<int>("UnidadesVendidas");

                    b.HasKey("Id");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("CompleteApiCoreEntity.Common.Abilities.Classes.AbilitiEmployee", b =>
                {
                    b.HasOne("CompleteApiCoreEntity.Common.Abilities.Classes.Ability", "Ability")
                        .WithMany("AbilitiEmployees")
                        .HasForeignKey("AbilityId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CompleteApiCoreEntity.Common.Employees.Classes.Employee", "Employee")
                        .WithMany("AbilitiEmployees")
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CompleteApiCoreEntity.Common.Employees.Classes.Employee", b =>
                {
                    b.HasOne("CompleteApiCoreEntity.Common.Deparment.Classes.Department", "Deparment")
                        .WithMany("Employees")
                        .HasForeignKey("DepartmentId");
                });

            modelBuilder.Entity("CompleteApiCoreEntity.Common.Employees.Classes.EmployeeExpedient", b =>
                {
                    b.HasOne("CompleteApiCoreEntity.Common.Employees.Classes.Employee", "Employee")
                        .WithOne("EmployeeExpedient")
                        .HasForeignKey("CompleteApiCoreEntity.Common.Employees.Classes.EmployeeExpedient", "EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
