﻿using CompleteapiCoreBussines.Common;
using CompleteapiCoreBussines.Common.Opportunity;
using CompleteapiCoreBussines.Common.Products;
using CompleteapiCoreBussines.Common.Users;
using CompleteApiCoreDataAccess.Common.Employees;
using CompleteApiCoreDataAccess.Common.Opportunity;
using CompleteApiCoreDataAccess.Common.Products;
using CompleteApiCoreDataAccess.Common.Users;
using CompleteApiCoreEntity;
using CompleteApiCoreEntity.Common.Employees.Classes;
using CompleteApiCoreEntity.Common.Employees.Classes.Validators;
using CompleteApiCoreEntity.Common.Employees.Interface.Data;
using CompleteApiCoreEntity.Common.Employees.Interface.Service;
using CompleteApiCoreEntity.Common.Opportunity.Interface.Data;
using CompleteApiCoreEntity.Common.Opportunity.Interface.Service;
using CompleteApiCoreEntity.Common.Products.Interface.Data;
using CompleteApiCoreEntity.Common.Products.Interface.Service;
using CompleteApiCoreEntity.Common.Users.Interface.Data;
using CompleteApiCoreEntity.Common.Users.Interface.Service;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompleteApiCore
{
    public static class serviceCollectionScoped
    {
        public static void AddScopedServices(this IServiceCollection services)
        {  
            services.AddScoped<IOperationResult, OperationResult>();
            services.AddScoped<IServiceEmployee, ServiceEmployee>();
            services.AddScoped<IDataEmployee, DataEmployee>();
            services.AddScoped<IDataProducts, DataProducts>();
            services.AddScoped<IServiceProducts, ServiceProducts>();

            services.AddScoped<IDataOpportunity, DataOpportunity>();
            services.AddScoped<IServiceOpportunity, ServiceOpportunity>();

            services.AddScoped<IDataAccount, DataAccount>();
            services.AddScoped<IServiceAccount, ServiceAccount>();
        }

        public static void AddScopedValidationServices(this IServiceCollection services)
        {     
            services.AddTransient<IValidator<Employee>, EmployeeValidator>();
        }


    }
}
