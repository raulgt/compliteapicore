﻿using CompleteApiCoreEntity;
using CompleteApiCoreEntity.Common.Employees.Classes;
using CompleteApiCoreEntity.Common.Employees.Interface.Data;
using CompleteApiCoreEntity.Common.Employees.Interface.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace CompleteapiCoreBussines.Common
{
    public class ServiceEmployee : IServiceEmployee
    {
        private readonly IDataEmployee dataEmployee_;
        private readonly IOperationResult op_;


        public ServiceEmployee(IOperationResult op, IDataEmployee dataEmployee)
        {
            op_ = op;
            dataEmployee_ = dataEmployee;
        }

        public IOperationResult GetEmployees()
        {
            try
            {
                op_.GetSuccessOperation(dataEmployee_.GetEmployees());
            }
            
            catch (Exception e)
            {
                op_.GetErrorOperation(e);
            }


            return op_;
        }

        public IOperationResult SaveEmployee(Employee employee)
        {
            try
            {
                op_.GetSuccessOperation(dataEmployee_.SaveEmployee(employee));
            }

            catch (Exception e)
            {
                op_.GetErrorOperation(e);
            }


            return op_;
        }
    }
}
