﻿using CompleteApiCoreEntity;
using CompleteApiCoreEntity.Common.Products.Classes;
using CompleteApiCoreEntity.Common.Products.Interface.Data;
using CompleteApiCoreEntity.Common.Products.Interface.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;



namespace CompleteapiCoreBussines.Common.Products
{
    public class ServiceProducts : IServiceProducts
    {

        private readonly IOperationResult op;
        private readonly IDataProducts dataProducts;

        public ServiceProducts(IOperationResult _op, IDataProducts _dataProducts)
        {
            this.op = _op;
            this.dataProducts = _dataProducts;
        }


        public async Task<IOperationResult> CreateProductTableStore(ProductAzure productAzure)
        {
            try
            {
                op.GetSuccessOperation(await dataProducts.CreateProductTableStore(productAzure));
            }
            catch (Exception e)
            {

                op.GetErrorOperation(e);
            }
            return op;
        }


        public async Task<IOperationResult> DeleteProductTableStore(string rowKey)
        {
            try
            {
                op.GetSuccessOperation(await dataProducts.DeleteProductTableStore(rowKey));
            }
            catch (Exception e)
            {

                op.GetErrorOperation(e);
            }
            return op;
        }


        public async Task<IOperationResult> GetProductsTableStore(string nextRowKey, string nextPartitionKey, int bunch)
        {
            try
            {
                op.GetSuccessOperation(await dataProducts.GetProductsTableStore(nextRowKey, nextPartitionKey, bunch));
            }
            catch (Exception e)
            {

                op.GetErrorOperation(e);
            }
            return op;
        }

        public async Task<IOperationResult> GetProductTableStore(string rowKey)
        {
            try
            {
                op.GetSuccessOperation(await dataProducts.GetProductTableStore(rowKey));
            }
            catch (Exception e)
            {

                op.GetErrorOperation(e);
            }
            return op;
        }


        public async Task<IOperationResult> CreateProduct(Product product)
        {
            try
            {
               
                op.GetSuccessOperation(await dataProducts.CreateProduct(product));
            }

            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }      

        public async Task<IOperationResult> DeleteProduct(int id)
        {
            try
            {
                op.GetSuccessOperation(await dataProducts.DeleteProduct(id));
            }

            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }     

      

        public async Task<IOperationResult> GetProduct(int id)
        {
            try
            {
                op.GetSuccessOperation(await dataProducts.GetProduct(id));

            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }        

        public async Task<IOperationResult> GetProducts()
        {
            try
            {
                op.GetSuccessOperation(await dataProducts.GetProducts());

            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }       
    }
}
