﻿using CompleteApiCoreEntity;
using CompleteApiCoreEntity.Common.Users.Clasess;
using CompleteApiCoreEntity.Common.Users.Interface.Data;
using CompleteApiCoreEntity.Common.Users.Interface.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CompleteapiCoreBussines.Common.Users
{
    public class ServiceAccount : IServiceAccount
    {

        private readonly IOperationResult op;
        private readonly IDataAccount dataAccount;


        public ServiceAccount(IOperationResult _op, IDataAccount _dataAccount)
        {
            op = _op;
            dataAccount = _dataAccount;
        }

        public async Task<IOperationResult> CreateUser(UserInfo userInfo)
        {
            try
            {
                op.GetSuccessOperation(await dataAccount.CreateUser(userInfo));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }
            return op;
        }

        public async Task<IOperationResult> LoginUser(UserInfo userInfo)
        {
            try
            {
                op.GetSuccessOperation(await dataAccount.LoginUser(userInfo));
            }
            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }
            return op;
        }
    }
}
