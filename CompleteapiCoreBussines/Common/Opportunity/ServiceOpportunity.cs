﻿using CompleteApiCoreEntity;
using CompleteApiCoreEntity.Common.Opportunity.Interface.Data;
using CompleteApiCoreEntity.Common.Opportunity.Interface.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CompleteapiCoreBussines.Common.Opportunity
{
    public class ServiceOpportunity : IServiceOpportunity
    {

        private readonly IDataOpportunity dataOpportunity;
        private readonly IOperationResult op;

        public ServiceOpportunity(IOperationResult _op, IDataOpportunity _dataOpportunity)
        {
            op = _op;
            dataOpportunity = _dataOpportunity;
        }

        public async Task<IOperationResult> ChangeStatusOpportunity(string user)
        {
            try
            {
                op.GetSuccessOperation(await dataOpportunity.ChangeStatusOpportunity(user));
            }

            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }

        public async Task<IOperationResult> GetStatusOpportunity()
        {
            try
            {
                op.GetSuccessOperation(await dataOpportunity.GetStatusOpportunity());
            }

            catch (Exception e)
            {
                op.GetErrorOperation(e);
            }

            return op;
        }
    }
}
