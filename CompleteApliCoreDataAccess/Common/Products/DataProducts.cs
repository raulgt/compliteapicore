﻿using CompleteApiCoreEntity.Common.DataBase.Class;
using CompleteApiCoreEntity.Common.Products.Classes;
using CompleteApiCoreEntity.Common.Products.Interface.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using CompleteApiCoreEntity.Common.Products.Classes.Validators;
using System.Linq;
using System.Linq.Expressions;

namespace CompleteApiCoreDataAccess.Common.Products
{
    public class DataProducts : IDataProducts
    {

        readonly DBEntitiesContext dbContext;

        public DataProducts(DBEntitiesContext _dbContext)
        {
            dbContext = _dbContext;
        }

        //////////////////// TABLE STORE ///////////////////
        ///
        static string partitionKey = Environment.GetEnvironmentVariable("PARTITIONKEY");
        static string accountName = Environment.GetEnvironmentVariable("ACCOUNTNAME");

        CloudStorageAccount storageAccount = new CloudStorageAccount(
                                             new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(
                                                 "123459876", accountName), true);


        public async Task<bool> CreateProductTableStore(ProductAzure productAzure)
        {
            try
            {

                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable productTable = tableClient.GetTableReference("productsvalera");

                TableOperation insertOperation = TableOperation.Insert(productAzure);
                var res = await productTable.ExecuteAsync(insertOperation);

                if (res != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<bool> DeleteProductTableStore(string rowKey)
        {
            try
            {

                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable productTable = tableClient.GetTableReference("productsvalera");

                TableOperation retrieveOperation = TableOperation.Retrieve<ProductAzure>(partitionKey, rowKey);

                TableResult retrievedResult = await productTable.ExecuteAsync(retrieveOperation);

                // Assign the result to a CustomerEntity object.
                ProductAzure deleteEntity = (ProductAzure)retrievedResult.Result;

                // Create the Delete TableOperation and then execute it.
                if (deleteEntity != null)
                {
                    TableOperation deleteOperation = TableOperation.Delete(deleteEntity);


                    // Execute the operation.
                    var res = await productTable.ExecuteAsync(deleteOperation);

                    if (res != null)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<List<ProductAzure>> GetProductsTableStore(string nextRowKey, string nextPartitionKey, int bunch)
        {
            try
            {
                int iterador = bunch;

                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable productTable = tableClient.GetTableReference("productsvalera");

                if (bunch > 0)
                {
                    TableQuery<ProductAzure> query = new TableQuery<ProductAzure>()
                        .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey))
                        .Take(bunch);

                    TableContinuationToken token;

                    if (nextRowKey == "empty")
                    {
                        token = null;
                    }
                    else
                    {
                        token = new TableContinuationToken();
                        token.NextRowKey = nextRowKey;
                        token.NextPartitionKey = nextPartitionKey;
                    }
                    List<ProductAzure> productList = new List<ProductAzure>();
                    //do
                    //{

                    TableQuerySegment<ProductAzure> resultSegment = await productTable.ExecuteQuerySegmentedAsync(query, token);
                    token = resultSegment.ContinuationToken;

                    foreach (ProductAzure entity in resultSegment.Results)
                    {

                        entity.NextRowKey = (token == null) ? "empty" : token.NextRowKey;
                        entity.NextPartitionKey = (token == null) ? "empty" : token.NextPartitionKey;

                        productList.Add(new ProductAzure()
                        {
                            Nombre = entity.Nombre,
                            Caracteristicas = entity.Caracteristicas,
                            FechaLanzamiento = entity.FechaLanzamiento,
                            FechaUltimaRevision = entity.FechaUltimaRevision,
                            Revision = entity.Revision,
                            CorreoFabricante = entity.CorreoFabricante,
                            Pais = entity.Pais,
                            Precio = entity.Precio,
                            UnidadesDisponibles = entity.UnidadesDisponibles,
                            UnidadesVendidas = entity.UnidadesVendidas,
                            ImagenFileTable = entity.ImagenFileTable,
                            NextRowKey = entity.NextRowKey,
                            NextPartitionKey = entity.NextPartitionKey,
                            RowKey = entity.RowKey,
                        });
                    }

                    return productList;
                    //} while (token != null);
                }
                else
                {
                    TableQuery<ProductAzure> query = new TableQuery<ProductAzure>()
                        .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey));

                    TableContinuationToken token = null;
                    TableQuerySegment<ProductAzure> resultSegment = await productTable.ExecuteQuerySegmentedAsync(query, token);
                    return resultSegment.Results;
                }

            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<ProductAzure> GetProductTableStore(string rowKey)
        {
            try
            {

                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable productTable = tableClient.GetTableReference("productsvalera");

                TableOperation retrieveOperation = TableOperation.Retrieve<ProductAzure>(partitionKey, rowKey);

                TableResult retrievedResult = await productTable.ExecuteAsync(retrieveOperation);

                // Assign the result to a CustomerEntity object.
                ProductAzure lookingEntity = (ProductAzure)retrievedResult.Result;

                if (lookingEntity != null)
                {
                    return lookingEntity;
                }

                return null;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }


        ///////////////////////////////////////////////////////////////////////////////
        ///                    CRUD SQLSERVER                                      ///
        /////////////////////////////////////////////////////////////////////////////


        public async Task<bool> CreateProduct(Product product)
        {
            try
            {
                dbContext.Products.Add(product);

                var result = await dbContext.SaveChangesAsync();

                return result > 0;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }


        public async Task<bool> DeleteProduct(int id)
        {
            try
            {
                var model = await dbContext.Products.FindAsync(id);
                dbContext.Products.Remove(model);
                var result = await dbContext.SaveChangesAsync();
                return result > 0;
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<Product> GetProduct(int id)
        {
            try
            {


                return await dbContext.Products.FindAsync(id);
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public async Task<List<Product>> GetProducts()
        {
            try
            {
                // Retorna todos los viajes disponibles comunmente es necesario implementar un metodo de paginacion
                return await dbContext.Products.ToListAsync();
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }


    }
}
