﻿using CompleteApiCoreEntity.Common.DataBase.Class;
using CompleteApiCoreEntity.Common.Employees.Classes;
using CompleteApiCoreEntity.Common.Employees.Interface.Data;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Text;
using CompleteApiCoreEntity.Common.Employees.Classes.Validators;
using FluentValidation.Results;

namespace CompleteApiCoreDataAccess.Common.Employees
{
    public class DataEmployee : IDataEmployee
    {

        DBEntitiesContext  dbContext_;
   

        public DataEmployee(DBEntitiesContext dbContext)
        {
            dbContext_ = dbContext;
        }

        public List<Employee> GetEmployees()
        {
            try
            {
                List<Employee> employees = dbContext_.Employees.ToList();                

                return employees;

            } catch (Exception e)
            {

                throw e.InnerException ?? e;
            }  
        }

        public bool SaveEmployee(Employee employee)
        {
            try
            {

                    dbContext_.Employees.Add(employee);

                int s = dbContext_.SaveChanges();

                return (s > 0)? true : false;

            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }
    }
}
