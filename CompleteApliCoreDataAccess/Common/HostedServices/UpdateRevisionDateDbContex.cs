﻿using CompleteApiCoreEntity.Common.DataBase.Class;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CompleteApiCoreDataAccess.Common.HostedServices
{
    public class UpdateRevisionDateDbContex : IHostedService, IDisposable
    {
        public IServiceProvider Services;
        private Timer _timer;

        public UpdateRevisionDateDbContex(IServiceProvider services)
        {
            Services = services;
        }      

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(1));

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            using (var scope = Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DBEntitiesContext>();
                var productModel = context.Products.Where(x => x.Revision == true).ToList();

                foreach (var product in productModel)
                {
                    product.FechaUltimaRevision = DateTime.Now;
                }
                context.UpdateRange(productModel);
                context.SaveChanges();                
            }
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
