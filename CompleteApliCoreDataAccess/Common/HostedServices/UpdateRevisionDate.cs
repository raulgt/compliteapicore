﻿using CompleteApiCoreEntity.Common.Products.Classes;
using Microsoft.Extensions.Hosting;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CompleteApiCoreDataAccess.Common.HostedServices
{
    public class UpdateRevisionDate : IHostedService, IDisposable
    {
        private readonly IHostingEnvironment environment;
        private Timer timer;


        public UpdateRevisionDate(IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        static string partitionKey = Environment.GetEnvironmentVariable("PARTITIONKEY");
        static string accountName = Environment.GetEnvironmentVariable("ACCOUNTNAME");

        CloudStorageAccount storageAccount = new CloudStorageAccount(
                                             new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(
                                                 "123459876", accountName), true);



        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new Timer(UpdatingRevision, null, TimeSpan.Zero, TimeSpan.FromMinutes(1));
            

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {

            timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }


        public async void UpdatingRevision(object state)
        {
            try
            {
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable productTable = tableClient.GetTableReference("productsvalera");

                // Obtiene todos las entidades donde revision es igual a True
                TableQuery<ProductAzure> query = new TableQuery<ProductAzure>()
                    .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey))
                    .Where(TableQuery.GenerateFilterConditionForBool("Revision", QueryComparisons.Equal, true));

                TableContinuationToken token = null;
                TableQuerySegment<ProductAzure> resultSegment = await productTable.ExecuteQuerySegmentedAsync(query, token);
                var entidadesRevision = resultSegment.Results;


                // modifica la propiedad "FechaUltimaRevision" en cada entidad de el listado dado
                foreach (var revision in entidadesRevision)
                {
                    var entity = new DynamicTableEntity(partitionKey, revision.RowKey);
                    entity.ETag = "*";
                    entity.Properties.Add("FechaUltimaRevision", new EntityProperty(DateTime.Now));
                    var mergeOperation = TableOperation.Merge(entity);
                    await productTable.ExecuteAsync(mergeOperation);
                }
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public void Dispose()
        {
            timer?.Dispose();
        }
    }
}
