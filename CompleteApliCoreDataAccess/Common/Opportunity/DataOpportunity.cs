﻿using CompleteApiCoreEntity.Common.DataBase.Class;
using CompleteApiCoreEntity.Common.Opportunity.Interface.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;

namespace CompleteApiCoreDataAccess.Common.Opportunity
{
    public class DataOpportunity : IDataOpportunity
    {

        readonly DBEntitiesContext dbContext;

        public DataOpportunity(DBEntitiesContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public async Task<bool> ChangeStatusOpportunity(string user)
        {
            try
            {
                var model = dbContext.JobOpportunities.FirstOrDefault();
                model.Open = (model.Open) ? false : true;
                model.User = user;
                model.LastModification = DateTime.Now;

                var result = await dbContext.SaveChangesAsync();

                if (result > 0)
                {
                    return model.Open;
                }
                else
                {
                    throw new Exception("No se logro modificar el status");
                }
                
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        public Task<bool> GetStatusOpportunity()
        {
            try
            {
                return Task.Run(() =>
                {
                    var model = dbContext.JobOpportunities.FirstOrDefault();
                    bool status = model.Open;

                    return status;
                });
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }
    }
}
