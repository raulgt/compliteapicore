﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CompleteApiCoreDataAccess.Common.Hubs
{
    public class OpportunityHub :  Hub
    {
        public async Task NotifyOpportunity(bool status)
        {
            await Clients.All.SendAsync("ReceiveStatus", status);
        }
    }
}
