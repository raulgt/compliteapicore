﻿using CompleteApiCoreEntity.Common.Users.Clasess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CompleteApiCoreEntity.Common.Users.Interface.Data
{
    public interface IDataAccount
    {
        Task<TokenInfo> CreateUser(UserInfo userInfo);

        Task<TokenInfo> LoginUser(UserInfo userInfo);
    }
}
