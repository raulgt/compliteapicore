﻿using CompleteApiCoreEntity.Common.Users.Clasess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CompleteApiCoreEntity.Common.Users.Interface.Service
{
    public interface IServiceAccount
    {
        Task<IOperationResult> CreateUser(UserInfo userInfo);

        Task<IOperationResult> LoginUser(UserInfo userInfo);
    }
}
