﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompleteApiCoreEntity.Common.Users.Clasess
{
    public class TokenInfo
    {
        public string Token { get; set; }

        public DateTime Expiration { get; set; }
    }
}
