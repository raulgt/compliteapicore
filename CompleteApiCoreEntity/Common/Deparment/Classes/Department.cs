﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CompleteApiCoreEntity.Common.Employees.Classes;

namespace CompleteApiCoreEntity.Common.Deparment.Classes
{
    public class Department
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }

       
        public List<Employee>  Employees { get; set; }
    }
}
