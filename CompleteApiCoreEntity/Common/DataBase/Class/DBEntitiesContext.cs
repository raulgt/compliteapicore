﻿using CompleteApiCoreEntity.Common.Employees.Classes;
using CompleteApiCoreEntity.Common.Deparment.Classes;
using Microsoft.EntityFrameworkCore;
using CompleteApiCoreEntity.Common.Abilities.Classes;
using CompleteApiCoreEntity.Common.Products.Classes;
using CompleteApiCoreEntity.Common.Opportunity.Classes;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace CompleteApiCoreEntity.Common.DataBase.Class
{
    public class DBEntitiesContext : IdentityDbContext<ApplicationUser>
    {

        public DBEntitiesContext(DbContextOptions options) : base(options)
        {
            
        }

      
        protected override void OnModelCreating(ModelBuilder builder)
        {
            
            // Definimos el join entre el modelo AbilitiEmployee - Employee - Ability
            builder.Entity<AbilitiEmployee>()
                .HasKey(ae => new { ae.AbilityId, ae.EmployeeId });

            builder.Entity<AbilitiEmployee>()
                .HasOne(ae => ae.Employee)
                .WithMany(e => e.AbilitiEmployees)
                .HasForeignKey(ae => ae.EmployeeId);

            builder.Entity<AbilitiEmployee>()
                .HasOne(ae => ae.Ability)
                .WithMany(a => a.AbilitiEmployees)
                .HasForeignKey(ae => ae.AbilityId);
            //////////
            
         
            
            base.OnModelCreating(builder);
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Deparments { get; set; }
        public DbSet<EmployeeExpedient> EmployeeExpedients { get; set; }
        public DbSet<Ability> Abilities { get; set; }
        public DbSet<AbilitiEmployee> AbilitiEmployees { get; set; }

        public DbSet<JobOpportunity> JobOpportunities { get; set; }
        public DbSet<Product> Products { get; set; }
        

    }
}
