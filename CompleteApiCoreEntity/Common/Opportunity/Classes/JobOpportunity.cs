﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompleteApiCoreEntity.Common.Opportunity.Classes
{
    public class JobOpportunity
    {
        public int Id { get; set; }

        public bool Open { get; set; }      

        public DateTime LastModification { get; set; }

        public string JobLink { get; set; }

        public string User { get; set; }
    }
}
