﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CompleteApiCoreEntity.Common.Opportunity.Interface.Service
{
    public interface IServiceOpportunity
    {
        Task<IOperationResult> GetStatusOpportunity();

        Task<IOperationResult> ChangeStatusOpportunity(string user);
    }
}
