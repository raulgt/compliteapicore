﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CompleteApiCoreEntity.Common.Opportunity.Interface.Data
{
    public interface IDataOpportunity
    {    
        // Indica el estado actual
        Task<bool> GetStatusOpportunity();

        // Indica el estado luego del cambio
        Task<bool> ChangeStatusOpportunity(string user);
    }
}
