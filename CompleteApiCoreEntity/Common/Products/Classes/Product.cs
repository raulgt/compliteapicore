﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;



namespace CompleteApiCoreEntity.Common.Products.Classes
{
    public class Product
    {
       
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Caracteristicas { get; set; }

        public DateTime FechaLanzamiento { get; set; }

        public string CorreoFabricante { get; set; }

        public string Pais { get; set; }

        public double Precio { get; set; }

        public int UnidadesDisponibles { get; set; }

        public int UnidadesVendidas { get; set; }

        // Utilizado en SQL Server
        public byte[] ImagenFile { get; set; }               

        public DateTime FechaUltimaRevision { get; set; }

        public bool Revision { get; set; }

        [NotMapped]      
        public FormFile Imagen { get; set; }


    }
}
