﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Text;

namespace CompleteApiCoreEntity.Common.Products.Classes
{
    public class ProductAzure : TableEntity
    {            

        public ProductAzure()
        {
            PartitionKey ="15952447";
            RowKey = Guid.NewGuid().ToString();
        }

        public string Nombre { get; set; }

        public string Caracteristicas { get; set; }

        public DateTime FechaLanzamiento { get; set; }

        public string CorreoFabricante { get; set; }

        public string Pais { get; set; }

        public double Precio { get; set; }

        public int UnidadesDisponibles { get; set; }

        public int UnidadesVendidas { get; set; }
   
        public byte[] ImagenFileTable { get; set; }        

        public DateTime FechaUltimaRevision { get; set; }

        public bool Revision { get; set; }

        public string NextRowKey { get; set; }

        public string NextPartitionKey { get; set; }
    }
}
