﻿using CompleteApiCoreEntity.Common.Products.Classes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CompleteApiCoreEntity.Common.Products.Interface.Data
{
    public interface IDataProducts
    {
        // Sql Server
        Task<List<Product>> GetProducts();

        Task<Product> GetProduct(int id);

        Task<bool> CreateProduct(Product product);

        Task<bool> DeleteProduct(int id);

        // Table Store
        Task<List<ProductAzure>> GetProductsTableStore(string nextRowKey, string nextPartitionKey, int bunch);

        Task<ProductAzure> GetProductTableStore(string rowKey);

        Task<bool> CreateProductTableStore(ProductAzure productAzure);

        Task<bool> DeleteProductTableStore(string rowKey);
    }
}
