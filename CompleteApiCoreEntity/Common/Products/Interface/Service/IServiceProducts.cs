﻿using CompleteApiCoreEntity.Common.Products.Classes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CompleteApiCoreEntity.Common.Products.Interface.Service
{
    public interface IServiceProducts
    {
        Task<IOperationResult> GetProducts();

        Task<IOperationResult> GetProduct(int id);

        Task<IOperationResult> CreateProduct(Product product);

        Task<IOperationResult> DeleteProduct(int id);

        // Table Store

        Task<IOperationResult> GetProductsTableStore(string nextRowKey, string nextPartitionKey, int bunch);

        Task<IOperationResult> GetProductTableStore(string rowKey);

        Task<IOperationResult> CreateProductTableStore(ProductAzure productAzure);

        Task<IOperationResult> DeleteProductTableStore(string rowKey);
    }
}
