﻿using CompleteApiCoreEntity.Common.DataBase.Interface;
using CompleteApiCoreEntity.Common.Employees.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace CompleteApiCoreEntity.Common.Employees.Interface.Data
{
    public interface IDataEmployee : IBaseModel<Employee>
    {
        List<Employee> GetEmployees();

        Boolean SaveEmployee(Employee employee);
    }
}
