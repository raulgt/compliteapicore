﻿using CompleteApiCoreEntity.Common.Employees.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace CompleteApiCoreEntity.Common.Employees.Interface.Service
{
    public interface IServiceEmployee
    {
        IOperationResult GetEmployees();

        IOperationResult SaveEmployee(Employee employee);
    }
}
