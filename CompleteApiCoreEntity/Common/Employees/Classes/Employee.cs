﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CompleteApiCoreEntity.Common.Deparment.Classes;
using System.Text;
using CompleteApiCoreEntity.Common.Abilities.Classes;

namespace CompleteApiCoreEntity.Common.Employees.Classes
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Prefix { get; set; }

        public string Position { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime HireDate { get; set; }

        [StringLength(300)]
        public string Notes { get; set; }

        public string Address { get; set; }

        // Relacion uno a muchos entre Empleado y departamento
        public int? DepartmentId { get; set; }
        public Department Deparment { get; set; }

        //Relacion uno a uno entre Empleado y Expediente Empleado
        public EmployeeExpedient EmployeeExpedient { get; set; }

        //Relacion muchos a muchos entre Ability y Empleado utilizando el model auxiliar AbilitiEmployee
        public List<AbilitiEmployee> AbilitiEmployees { get; set; }

    }
}
