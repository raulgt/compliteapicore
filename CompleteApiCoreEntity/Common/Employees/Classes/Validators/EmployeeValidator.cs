﻿using System;
using System.Collections.Generic;
using System.Text;
using CompleteApiCoreEntity.Common.DataBase.Class;
using CompleteApiCoreEntity.Common.Deparment.Classes;
using FluentValidation;
using FluentValidation.Results;
using System.Linq;
using System.Linq.Expressions;


namespace CompleteApiCoreEntity.Common.Employees.Classes.Validators
{
    public class EmployeeValidator : AbstractValidator<Employee>
    {
        DBEntitiesContext dbContext_;
        

        public EmployeeValidator(DBEntitiesContext dbContext)
        {
            dbContext_ = dbContext;

            RuleFor(e => e.FirstName)
                    .NotEmpty()
                    .NotEqual("DeadPool")
                    .WithMessage("El nombre no puede estar vacio o llamarse DeadPool");


            RuleFor(e => e.DepartmentId).Must(ValidateDepartment).WithMessage("Tecnologic deparment is no permited here..!!!");
        }

        private bool ValidateDepartment(int? deparmentId)
        {
            string description = dbContext_.Deparments.Where(x => x.Id == deparmentId).Select(x => x.Description).ToString();
            return description == "Tecnologies";
        }
    }
}
