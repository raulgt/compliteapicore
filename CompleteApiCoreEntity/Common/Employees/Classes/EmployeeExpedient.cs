﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompleteApiCoreEntity.Common.Employees.Classes
{
    public class EmployeeExpedient
    {
        public int Id { get; set; }

        public string Nationality { get; set; }

        public string Expedient { get; set; }

        // Relacion uno a muchos entre  EmployeeExpedient
        public int EmployeeId { get; set; }
        public Employee  Employee { get; set; }


    }
}
