﻿using CompleteApiCoreEntity.Common.Employees.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CompleteApiCoreEntity.Common.Abilities.Classes
{
    public class AbilitiEmployee
    {
        //Relacion muchos a muchos entre Ability y Empleado utilizando el model auxiliar AbilitiEmployee

        public int EmployeeId { get; set; }
        public Employee  Employee { get; set; }

        public int AbilityId { get; set; }
        public Ability  Ability { get; set; }
    }
}
