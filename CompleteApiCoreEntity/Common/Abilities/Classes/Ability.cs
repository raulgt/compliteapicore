﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CompleteApiCoreEntity.Common.Abilities.Classes
{
    public class Ability
    { 
        [Key]
        public int Id { get; set; }       

        public string Name { get; set; }

        public string Description { get; set; }

        //Relacion muchos a muchos entre Ability y Empleado utilizando el model auxiliar AbilitiEmployee
        public List<AbilitiEmployee> AbilitiEmployees { get; set; }
    }
}
