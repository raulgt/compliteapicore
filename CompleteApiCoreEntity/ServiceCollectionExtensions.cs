﻿using CompleteApiCoreEntity.Common.DataBase.Class;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CompleteApiCoreEntity
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDataAccessServices(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<DBEntitiesContext>(options =>
              options.UseSqlServer(connectionString, b => b.MigrationsAssembly("CompleteApiCore")));
        }
    }
}
