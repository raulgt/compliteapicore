﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace CompleteApiCoreEntity
{
    public interface IOperationResult
    {
        bool isSuccess { get; }
        object result { get; set; }
        object error { get; set; }
        HttpStatusCode status { get; set; }
        void GetSuccessOperation(Object result);
        void GetErrorOperation(Exception message, HttpStatusCode status = HttpStatusCode.InternalServerError);
    }

    public class OperationResult : IOperationResult
    {
      

        private HttpStatusCode _status = HttpStatusCode.OK;
        public HttpStatusCode status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        private IEnumerable<HttpStatusCode> Success = new List<HttpStatusCode>
        {
            HttpStatusCode.OK,
            HttpStatusCode.Created,
            HttpStatusCode.Accepted,
            HttpStatusCode.NonAuthoritativeInformation,
            HttpStatusCode.NoContent,
            HttpStatusCode.ResetContent,
            HttpStatusCode.PartialContent,
        };

        public bool isSuccess
        {
            get
            {
                return (Success.Contains(status));
            }
        }


        private Object _result;
        public Object result
        {
            get
            {
                if (isSuccess)
                    return _result;
                else
                    return null;
            }
            set { _result = value; }
        }


        private Object _error;
        public Object error
        {
            get
            {
                if (!isSuccess)
                    return _error;
                else
                    return null;
            }
            set { _error = value; }
        }


        public void GetErrorOperation(Exception message, HttpStatusCode status = HttpStatusCode.InternalServerError)
        {
            var handled_error = message.InnerException == null ? message.Message : message.InnerException.Message;


            this.status = status;
            this.error = handled_error;
        }


        public void GetSuccessOperation(object result)
        {
            if (result == null)
            {
                this.status = HttpStatusCode.NoContent;
            }
            else
            {
                this.status = HttpStatusCode.OK;
                this.result = result;
            }
        }


    }
}
