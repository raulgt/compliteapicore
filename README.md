### Codigo Fuente del Proyecto Servicio Rest para Crud con Table Storage y Sql Server

```
Pasos a seguir para ejecutar el proyecto en modo de desarrollo luego de clonar el repositorio: 

- En TABLE Storage
Ejecute el proyecto con una conexión a internet estable, la vista por defecto del api muestra el mensaje "SERVICIO FUNCIONANDO..!!!"


- En SQL SERVER

Crear una base de datos vacia en el servidor de SqlServer que se piensa utilizar llamada "Practicas".
Cheaquear el ConnectionStrings en el archivo "appsettings.json" dentro del proyecto "CompleteApiCore" modificar el nombre del servidor de base
de datos de ser necesario.

Ejecutar los siguientes comandos en orden en el Packeg Manager Console: 
1) add-migration firtsMigration -o "DataMigrations"
2) update-database



Ejecute el proyecto en IIS Express
Luego de estos pasos el servicio rest esta listo para ser utilizado..!!
```